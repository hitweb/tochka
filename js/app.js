//document.addEventListener('touchstart', onTouchStart, {passive: true});
//прокрутка элементов правого сайдбара при скроллинге
var sticky = document.getElementsByClassName('fixed');
for (var i = 0; i < sticky.length; i++) {
    new hcSticky(sticky[i], {
        stickTo: sticky[i].parentNode,
        top: 40,
        followScroll:false
    });
}

// $( ".more-title a" ).click(function() {
//     // var $sticky = $('.fixed');
//     // for (var $i = 0; $i < $sticky.length; $i++) {
//     //      hcSticky($sticky[$i])('refresh');
//     // }
// });
//
// function refreshstiky () {
//     document.getElementsByClassName('fixed').reset();
//     sticky = document.getElementsByClassName('fixed');
//     for (i = 0; i < sticky.length; i++) {
//         new hcSticky(sticky[i], {
//             stickTo: sticky[i].parentNode,
//             top: 40,
//             followScroll:false
//         });
//     }
// }




$('.more-title a').click(function () {
    if  ($(this).hasClass('collapsed'))  {
        $(this).text('Показать больше новостей');
    }
    else {
        $(this).text('Меньше новостей');
    }
});



var swiper = new Swiper ('.main-swiper', {
    // slidesPerView: 1,
    // spaceBetween: 10,
    loop: true,
    navigation: {
        nextEl: '.block2 .next',
        prevEl: '.block2 .prev'
    }
});
var trends = new Swiper ('.trends-items', {
    loop: true,
    slidesPerView: 'auto',
    navigation: {
        nextEl: '.double-right-shield',
    },
});

$(document).ready(function(){
    $('.sidenav').sidenav({});
    $( "#sidebarCollapse" ).click(function() {
        $('.sidenav').sidenav('open');
    });
});
var Tochka=Tochka||{};
Tochka.radio={};
Tochka.radio.popupOptions={
    toolbar:0,location:0,directories:0,scrollbars:1,resizable:0,status:0,height:700,width:815,top:50,left:50
};
Tochka.radio.initPopupOptions=function(){
    var screenHeight=screen.height;
    var screenWidth=screen.width;
    if(Tochka.radio.popupOptions.height>screenHeight){
        Tochka.radio.popupOptions.height=Math.max(300,screenHeight);
    }
    if(Tochka.radio.popupOptions.width>screenWidth){
        Tochka.radio.popupOptions.width=Math.max(300,screenWidth);
    }
    Tochka.radio.popupOptions.top=(screenHeight/2)-(Tochka.radio.popupOptions.height/2);Tochka.radio.popupOptions.left=(screenWidth/2)-(Tochka.radio.popupOptions.width/2);}
Tochka.radio.getPopupOptionsString=function(){
    var s=[];
    for(var name in Tochka.radio.popupOptions){s.push(name+'='+Tochka.radio.popupOptions[name]);}
    return s.join(',');
};
Tochka.radio.openPopup=function(url){
    Tochka.radio.initPopupOptions();
    window.open(url,'radio-popup',Tochka.radio.getPopupOptionsString()).focus();
};
var o = $('.rd-navbar');
$(document).ready(function () {
    o.RDNavbar({
         stickUpOffset: 170,
    });
});

$( ".comment_subscribe" ).click(function() {

    if($('.comment_subscribe input').is(':checked'))
    {
        $('.comment_subscribe .formst').addClass('active');
    }
    else{
        $('.comment_subscribe .formst').removeClass('active');
    }
});







(function(){
    var inf = document.getElementById('siteLinksUmhGroup');
    if(inf)
    {
        inf.innerHTML='' +
            '<div class="dropdown-list" onclick="$(this).parent().toggleClass(\'dropdown-class\')">Cайты UMHgroup' +
            '<div id="dropdown-1" class="f-dropdown dropdown-tip">'+
            '<ul class="dropdown-menu-umh">'+
            '<li><a href="http://i.ua"  rel="nofollow" target="_blank">I.ua</a></li>'+
            '<li><a href="http://bigmir.net"  rel="nofollow" target="_blank">bigmir)net</a></li>'+
            '<li><a href="https://korrespondent.net"  rel="nofollow" target="_blank">КорреспонденТ</a></li>'+
            '<li><a href="http://aif.ua"  rel="nofollow" target="_blank">Аргументы и факты в Украине</a></li>'+
            '<li><a href="https://kp.ua"  rel="nofollow" target="_blank">КП в Украине</a></li>'+
            '<li><a href="http://dengi.ua"  rel="nofollow" target="_blank">Деньги.ua</a></li>'+
            '<li><a href="https://football.ua"  rel="nofollow" target="_blank">Football.ua</a></li>'+
            '<li><a href="http://iSport.ua"  rel="nofollow" target="_blank">iSport.ua</a></li>'+
            '<li><a href="https://vgorode.ua"  rel="nofollow" target="_blank">В Городе</a></li>'+
            '<li><a href="https://gloss.ua"  rel="nofollow" target="_blank">Gloss.ua</a></li>'+
            '<li><a href="https://tv.ua"  rel="nofollow" target="_blank">TV.ua</a></li>'+
            '<li><a href="http://afisha.bigmir.net"  rel="nofollow" target="_blank">Afisha.bigmir.net</a></li>'+
            '<li><a href="https://tochka.net"  rel="nofollow" target="_blank">tochka.net</a></li>'+
            '<li><a href="http://ivona.bigmir.net"  rel="nofollow" target="_blank">Ivona.bigmir.net</a></li>'+
            '<li><a href="https://irunning.com.ua"  rel="nofollow" target="_blank">I & Running</a></li>'+
            '<li><a href="https://bikeandme.com.ua"  rel="nofollow" target="_blank">Bike & Me</a></li>'+
            '<li><a href="https://archidea.com.ua"  rel="nofollow" target="_blank">Архидея</a></li>'+
            '<li><a href="http://uzr.com.ua"  rel="nofollow" target="_blank">За Рулём Украина</a></li>'+
            '<li><a href="http://mport.ua"  rel="nofollow" target="_blank">Mport.ua</a></li>'+
            '<li><a href="https://smak.ua" rel="nofollow" target="_blank">Smak.ua</a></li>'+
            '<li><a href="http://foodandmood.com.ua" rel="nofollow" target="_blank">Food & Mood</a></li>'+
            '<li><a href="https://kolobok.ua" rel="nofollow" target="_blank">Kolobok.ua</a></li>'+
            '<li><a href="http://radioclub.ua"  rel="nofollow" target="_blank">Radioclub</a></li>'+
            '<li><a href="http://nrj.ua"  rel="nofollow" target="_blank">NRJ.ua</a></li>'+
            '<li><a href="http://loungefm.com.ua"  rel="nofollow" target="_blank">Lounge FM</a></li>'+
            '<li><a href="http://www.retro.ua"  rel="nofollow" target="_blank">Ретро FM</a></li>'+
            '<li><a href="http://radioclub.ua/radio/jamfm"  rel="nofollow" target="_blank">Джем FM</a></li>'+
            '<li><a href="http://www.avtoradio.ua"  rel="nofollow" target="_blank">Авторадио</a></li>'+
            '<li><a href="http://radiopyatnica.com.ua"  rel="nofollow" target="_blank">Радио Пятница</a></li>'+
            '</ul>' +
            '</div>' +
            '</div>';
    }
})();